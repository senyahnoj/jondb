<?php
namespace JonDb;

/**
 * database class
 */
class Db
{
    /**
     * @var \Mysqli instance of mysqli
     */
    protected $_mysqli;

    /**
     * @var Db single instance of self
     */
    protected static $_instance;

    /**
     * Protected constructor - singleton
     */
    protected function __construct()
    {
    }

    /**
     * Make a connection to MySQLi
     *
     * @param string $host     hostname
     * @param string $user     username
     * @param string $password password
     * @param string $database name of database
     * @return Db
     * @throws \Exception
     */
    public function connect($host, $user, $password, $database)
    {
        $mysqli = new \Mysqli($host, $user, $password, $database);
        if ($mysqli->connect_error) {
            throw new \Exception($mysqli->connect_errno . ' ' . $mysqli->connect_error);
        }
        $this->setMysqli($mysqli);
        return $this;
    }

    /**
     * Singleton method - returns instance of Db
     *
     * @return Db
     */
    public static function getInstance()
    {
        isset(self::$_instance) or self::$_instance = new Db();
        return self::$_instance;
    }

    /**
     * real escapes input
     *
     * @param string $str input string
     * @return string
     */
    public static function escape($str)
    {
        return static::getInstance()->getMysqli()->real_escape_string($str);
    }

    /**
     * Sets the instance of MySQLi
     *
     * @param \Mysqli $mysqli instance of Mysqli
     * @return Db
     */
    public function setMysqli(\Mysqli $mysqli)
    {
        $this->_mysqli = $mysqli;
        return $this;
    }

    /**
     * Returns instance of Mysqli
     *
     * @return \Mysqli
     */
    public function getMysqli()
    {
        return $this->_mysqli;
    }

    /**
     * Quote and escape string
     *
     * @param mixed $str string or date/time input
     * @return string
     */
    public static function quote($str)
    {
        if ($str instanceof \DateTime) {
            $str = $str->format('Y-m-d H:i:s');
        }
        return "'" . static::escape($str) . "'";
    }
}
