<?php
namespace JonDb;

/**
 * database query class
 */
class Query
{
    /**
     * SQL statement
     *
     * @var string
     */
    protected $_sql;

    /**
     * Query resource
     *
     * @var Mysqli_Result
     */
    protected $_result;

    /**
     * @var Db instance of the DB
     */
    protected $_db;

    /**
     * Constructor - sets up SQL and runs query against database
     *
     * @param string $sql query
     */
    public function __construct($sql = null)
    {
        if (isset($sql)) {
            $this->run($sql);
        }
    }

    /**
     * Runs the given SQL statement against the database and returns boolean
     *
     * @param string $sql query
     * @return boolean
     */
    public function run($sql)
    {
        return $this->_setSql($sql)->_query();
    }

    /**
     * Sets this->_sql
     *
     * @param string $sql
     * @return Query
     */
    protected function _setSql($sql)
    {
        $this->_sql = $sql;
        return $this;
    }

    /**
     * Returns the SQL statement
     *
     * @return string
     */
    public function getSql()
    {
        return $this->_sql;
    }

    /**
     * Sets the instance of the DB
     *
     * @param Db $db the instance of the DB
     * @return Query
     */
    public function setDb(Db $db)
    {
        $this->setDb($db);
        return $this;
    }

    /**
     * Returns instance of the DB (lazy-loading)
     *
     * @return Db
     */
    public function getDb()
    {
        isset($this->_db) or $this->_db = Db::getInstance();
        return $this->_db;
    }

    /**
     * Runs the SQL query
     *
     * @return boolean
     */
    protected function _query()
    {
        $mysqli = $this->getDb()->getMysqli();
        if (!$this->_result = $mysqli->query($this->_sql)) {
            throw new \Exception($mysqli->errno . ' ' . $mysqli->error);
            return false;
        }
        return true;
    }

    /**
     * Returns the mysqli object from the parent connection
     *
     * useful things like ->insert_id, ->affected_rows
     * @return \Mysqli
     */
    public function getMysqli()
    {
        return $this->getDb()->getMysqli();
    }

    /**
     * Returns the last insert id
     *
     * @return int
     */
    public function getInsertId()
    {
        return $this->getMysqli()->insert_id;
    }

    /**
     * Fetch row as an associative array
     *
     * @param int $resultType MYSQLI_ASSOC, MYSQLI_NUM or MYSQLI_BOTH
     * @return array
     */
    public function fetch($resultType = \MYSQLI_ASSOC)
    {
        return $this->_result->fetch_array($resultType);
    }

    /**
     * Fetches all results as an index of associative arrays
     *
     * @param int $resultType MYSQLI_ASSOC, MYSQLI_NUM or MYSQLI_BOTH
     * @return array
     */
    public function fetchAll($resultType = \MYSQLI_ASSOC)
    {
        $results = array();
        while ($row = $this->fetch($resultType)) {
            $results[] = $row;
        }
        return $results;
    }

    /**
     * Returns field information as an array of objects
     *
     * @return array
     */
    public function fetchFields()
    {
        return $this->_result->fetch_fields();
    }

    /**
     * Returns the mysqli result object
     * you can get useful things like ->num_rows
     *
     * @return \Mysqli_Result
     */
    public function getResult()
    {
        return $this->_result;
    }

    /**
     * Returns the number of rows returned by the query
     *
     * @return int
     */
    public function getNumRows()
    {
        return $this->_result->num_rows;
    }
}
