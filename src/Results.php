<?php
namespace JonDb;

use Memcached;

/**
 * simple db class with higher level interface for common result set patterns
 */
class Results
{
    /**
     * @var Memcached instance of memcached
     */
    protected $memcached;

    /**
     * @var string prefix for all memcached strings
     */
    protected $cachePrefix;

    /**
     * Returns a new Query instance
     *
     * @param string $sql optional SQL constructor argument
     * @return Query
     */
    protected function _newQuery($sql = null)
    {
        return new Query($sql);
    }

    /**
     * Returns a single value
     *
     * @param string $sql      SQL statement
     * @param string $cacheKey memcached key
     * @return string
     */
    public function getOne($sql, $cacheKey = null)
    {
        if (isset($cacheKey) && ($cache = $this->fetchFromCache($cacheKey)) !== false) {
            return $cache;
        }

        $query = $this->_newQuery($sql);
        $row = $query->fetch(\MYSQLI_NUM);

        if (isset($cacheKey)) {
            $this->writeToCache($cacheKey, $row[0]);
        }

        return $row[0];
    }

    /**
     * Returns a single row
     *
     * @param string $sql SQL  statement
     * @param string $cacheKey memcached key
     * @return array
     */
    public function getRow($sql, $cacheKey = null)
    {
        if (isset($cacheKey) && ($cache = $this->fetchFromCache($cacheKey)) !== false) {
            return $cache;
        }

        $query = $this->_newQuery($sql);
        $row = $query->fetch(\MYSQLI_ASSOC);
        if (isset($cacheKey)) {
            $this->writeToCache($cacheKey, $row);
        }

        return $row;
    }

    /**
     * Returns an indexed associative array of all results
     *
     * @param string $sql      SQL statement
     * @param string $key      field to index by
     * @param string $cacheKey memcached key
     * @return array
     */
    public function getAll($sql, $key = null, $cacheKey = null)
    {
        if (isset($cacheKey) && ($cache = $this->fetchFromCache($cacheKey)) !== false) {
            return $cache;
        }

        $query = $this->_newQuery($sql);
        $results = array();
        while ($row = $query->fetch(\MYSQLI_ASSOC)) {
            if (!empty($key)) {
                $results[$row[$key]] = $row;
                unset($results[$row[$key]][$key]);
            } else {
                $results[] = $row;
            }
        }

        if (isset($cacheKey)) {
            $this->writeToCache($cacheKey, $results);
        }

        return $results;
    }

    /**
     * Returns an indexed single-dimensional array of results
     *
     * @param string $sql      SQL statement
     * @param string $cacheKey memcached key
     * @return array
     */
    public function getList($sql, $cacheKey = null)
    {
        if (isset($cacheKey) && ($cache = $this->fetchFromCache($cacheKey)) !== false) {
            return $cache;
        }

        $query = $this->_newQuery($sql);
        $results = array();
        while ($row = $query->fetch(\MYSQLI_NUM)) {
            $results[] = $row[0];
        }

        if (isset($cacheKey)) {
            $this->writeToCache($cacheKey, $results);
        }

        return $results;
    }

    /**
     * Updates the database
     *
     * @param string $sql      SQL statement
     * @param string $cacheKey memcached key
     * @return boolean
     */
    public function update($sql, $cacheKey = null)
    {
        if (isset($cacheKey)) {
            $this->invalidateCache($cacheKey);
        }

        $query = $this->_newQuery();
        return $query->run($sql);
    }

    /**
     * Runs a delete query
     *
     * @param string $sql      SQL statement
     * @param string $cacheKey memcached key
     * @return boolean
     */
    public function delete($sql, $cacheKey = null)
    {
        if (isset($cacheKey)) {
            $this->invalidateCache($cacheKey);
        }

        $query = $this->_newQuery();
        return $query->run($sql);
    }

    /**
     * Inserts into the database. returns last_insert_id
     *
     * @param string $sql SQL statement
     * @return int
     */
    public function insert($sql, $cacheKey = null)
    {
        if (isset($cacheKey)) {
            $this->invalidateCache($cacheKey);
        }

        $query = $this->_newQuery();
        $query->run($sql);
        return $query->getInsertId();
    }

    /**
     * Sets up instance of memcached
     *
     * @param Memcached $memcached instance of memcached
     * @param string    $prefix    string prefix for all memcached keys
     * @return Results
     */
    public function setMemcached(Memcached $memcached, $prefix = null)
    {
        $this->memcached = $memcached;
        $this->cachePrefix = $prefix;
        return $this;
    }

    /**
     * Invalidates the given cache key
     *
     * @param string $key key
     * @return Results
     */
    public function invalidateCache($key)
    {
        if (isset($this->memcached)) {
            $this->memcached->delete($this->cachePrefix . $key);
        }
        return $this;
    }

    /**
     * Fetches the given query results from memcached
     *
     * @param string $key key
     * @return mixed|false
     */
    protected function fetchFromCache($key)
    {
        if (isset($this->memcached)) {
            return $this->memcached->get($this->cachePrefix . $key);
        }
        return false;
    }

    /**
     * Writes the given data to memcached
     *
     * @param string $key  key
     * @param mixed  $data data
     * @return Results
     */
    protected function writeToCache($key, $data)
    {
        if (isset($this->memcached)) {
            $this->memcached->add($this->cachePrefix . $key, $data);
        }
    }
}
